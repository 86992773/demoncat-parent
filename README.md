# 使用

1、安装maven，使用 doc/maven-settings中的配置（执行deploy的用户需要配置<password>）。

2、本项目为根项目，即所有项目的父工程，负责所有依赖和插件的声明

# maven-build命令 

	清除：		clean
	下载依赖： 	dependency:copy-dependencies
	打包：		package  ;  clean package ; clean package -Dmaven.test.skip=true 
	安装： 		install  ;  clean install ; clean install -Dmaven.test.skip=true
	部署： 		deploy  ;  clean deploy ; clean deploy -Dmaven.test.skip=true

# pom-dependency-scope选项  

	compile：		默认，作用于所有阶段。
	test：			作用于测试阶段，不会被发布，如Junit
	provided：		作用于所有阶段，由JDK、容器或使用者提供依赖，如servlet.jar。 
	runtime：		作用于测试和运行阶段，如JDBC驱动。
	system：		作用于所有阶段，需显式提供依赖的jar，不会在Repository中查找
	
	注：pom中声明了非compile的jar包，因为没有被编译，所以被依赖时会忽略此jar，依赖者需重复依赖

# pom-dependency-optional选项

    <optional>true</optional>
    
    表示为可选依赖，例如JDBC封装工具时可以编写多个数据源的代码，但打包时忽略而不报错，由依赖方添加所需的依赖。
		
# Eclipse中启动spring-boot 

	1、在web项目上执行 mvn spring-boot:run 
	
	2、 运行App.main()
	
# Linux-JAR启动spring-boot

	nohup java -jar xxx.jar > catalina.out  2>&1 & 

	后台启动，输出控制台到catalina.out文件，tail -f catalina.out查看控制台，退出Shell时不关闭。

	nohup java -jar demoncat-seckill-moke.war --server.port=8610 > catalina.out  2>&1 &
	jar启动时，可以使用 --xx 指定application.properties中的配置参数。

# 普通spring-boot jar包install 

	在项目或聚合工程上执行 mvn install
	
	在父工程中，没有定义spring-boot部署相关的环境，所以可以直接install

# 版本管理

注：parent>version为必须配置；version / parent>version 不能使用${properties}，必须明文配置

1、顶级项目 demoncat-parent

	配置version
	
	配置全局依赖版本
	
	配置各项目/模块的依赖版本
	
2、项目 demoncat-xxx

	配置parent依赖顶级项目，继承项级项目的所有依赖版本
	
	配置version
	
3、模块 demoncat-xxx-yyy

	配置parent依赖项目，继承项目的版本（项目及模块统一版本）和顶级项目的所有依赖版本
	
4、统一修改项目和模块的依赖版本（被统一修改的项目必须配置version，模块则继承项目的version而不单独配置）

	mvn versions:set -DnewVersion=0.0.1-SNAPSHOT
	
5、如果项目下的模块不是通用模块，或暂时不想交给顶级项目管理，可以临时在项目上定义局部依赖管理
	
	1、项目的pom配置properties>xxx.version和dependencyManagement>dependencies

	2、项目下的模块间依赖不再指定版本，方便将来统一切换和管理
	
	3、如果有必要，再将项目局部依赖管理配置迁移到顶级项目中全局管理

6、如果是一组项目，并且不想交给顶级项目管理，也可以创建单独的dependencies项目来管理系列项目的依赖
	   
	1、创建xxx-dependencies项目，配置properties和dependencyManagement来统一定义系列项目的依赖版本
	
	2、在顶级项目的dependencies中依赖xxx-dependencies，参考spring-cloud-dependencies
	
	3、项目继承顶级项目，顶级项目包含xxx-dependencies，就相当于在顶级项目中管理了系统项目的依赖版本

 	

 	  
   
   