查看提交状态

    git status

查看提交日志(commitId)

    git log

更新代码

    git pull <origin branchName>

提交代码：缓存，提交，同步，强制同步

    git add .
    git commit -m "comments"
    git push <origin branchName>
    git push -f origin master

回滚代码

    git reset --hard <branchName> <commitId>

分支管理：查看，切换

    git branch -a
    git checkout branchName

标签管理：列表，查看(commitId)

    git tag
    git show tagName
  
添加标签：添加，同步

    git tag tagName
    git push origin tagName
  
删除标签

    git tag -d tagName
    git push origin --delete tagName

回滚标签

    git show tagName
    git reset --hard commitId

克隆项目

    git clone http://username:password@git.ys360.net/centec/centec-web.git
    git clone http://git.ys360.net/centec/centec-web.git

设置用户

    git config user.name "86992773"
    git config user.email "86992773@qq.com"

GitHub的HOST配置

    140.82.112.3 github.com
