# 打包
	
	jar cvf xxx.jar *
	将当前目录的文件打包成xxx.jar，显示打包过程。
	
# 打包(常用)
	
	jar cvfM0 xxx.jar *
	将当前目录的文件打包成xxx.jar，打包时不压缩、不创建META-INF信息，显示打包过程。
	
# 查看包的内容
	
	jar tvf xxx.jar
	jar tvf xxx.war | grep demoncat
	
# 解包
	
	jar xvf xxx.jar
	解压xxx.jar到当前目录。
	
# 添加文件
	
	jar uvf xxx.jar yyy.class
	将yyy.class添加到xxx.jar的根目录中，如果文件存在则替换。
	
# 添加文件
	
	mkdir WEB-INF/lib/ -p
	mv yyy.jar WEB-INF/lib/
	jar uvf xxx.jar WEB-INF/lib/yyy.jar
	【先创建替换文件的层级目录并放进去】将yyy.jar添加到xxx.jar的WEB-INF/lib目录中，如果文件存在则替换。
	